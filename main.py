# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 23:03:32 2022

@author: Lenovo
"""

if __name__ == "__main__": 
    from connect_DB import DB
    # from calculate_distance import CalculateDistance 
    # from time_travel import TimeTravel 
    from calculation_function import CalculationFunction
    import time
    import datetime
    import pandas as pd
    
    start_time = time.time()
    
    host='34.101.158.193'
    database='tetraflexlogdb'
    user='radioview'
    password='radioview@2022'
    
    # connect to database
    db = DB(host, database, user, password)
    
    def get_data(db, last_date):
        
        last_date = "'" + str(last_date) + "'"
        # get all data after last datetime
        # filter the data with datalength is not 'NODATA' and minimum speed is 5 kmh  
        query = "SELECT * FROM sdsdata_r_30d_bc" + \
            " WHERE callingssi IN (SELECT ISSI FROM mst_unitsupport_bc)" + \
            " AND timestamps >=" + last_date + \
            " AND datalength <> 'NODATA'" + \
            " AND speed_kmh >= 5"
            
        df_data = db.get_data(query)
        df_data.columns = ["datalength", "DbId", "timestamps", "calledorgdescr", "callingssi", "CallingDescr", "longitude", "latitude", "altitude_meter", "traveldirection", "positionerror_meter", "speed_kmh", "remark", "groupdate"]
        df_data.sort_values(by=['timestamps'], inplace=True)
        df_data["speed_kmh"] = pd.to_numeric(df_data["speed_kmh"], errors='coerce', downcast="float")
        df_data = df_data.dropna(subset = ['speed_kmh'])
        
        return df_data
    
    # delete data after 3 month in sdsdata_r_3m_distance_bc table
    query = "DELETE FROM sdsdata_r_3m_distance_bc WHERE timestamps_1 <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -3 MONTH), '%Y-%m-%d 00:00:00')"
    db.action_row_query(query)
    
    # delete data after 3 month in sdsdata_r_3m_speedlimit_bc table
    query = "DELETE FROM sdsdata_r_3m_speedlimit_bc WHERE timestamps <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -3 MONTH), '%Y-%m-%d 00:00:00')"
    db.action_row_query(query)
    
    # delete data after 3 month in sdsdata_r_3m_travel_bc table
    query = "DELETE FROM sdsdata_r_3m_travel_bc WHERE arrival_time <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -3 MONTH), '%Y-%m-%d 00:00:00')"
    db.action_row_query(query)
        
    # get last datetime 
    query = "SELECT GREATEST(" + \
        "(SELECT MAX(timestamps_1) FROM sdsdata_r_3m_distance_bc), " + \
        "(SELECT MAX(timestamps_2) FROM sdsdata_r_3m_distance_bc)," + \
        "(SELECT MAX(departure_time) FROM sdsdata_r_3m_travel_bc), " + \
        "(SELECT MAX(arrival_time) FROM sdsdata_r_3m_travel_bc)" + \
        ")"

    last_date = db.get_single_data(query) 
    df = get_data(db, last_date)
    
    Calc = CalculationFunction()
    
    # calculate distance between vehicle
    try:
        df_data = df[df.speed_kmh >= 10]
        limit_distance = 100 #metres
        interval_time = 30 #in seconds
        output_calcdist =  Calc.run_calculate_distance(df_data, limit_distance, interval_time) 
        fieldname_calcdist = list(output_calcdist[1].columns.values)
        db.insert_to_db('sdsdata_r_3m_distance_bc', fieldname_calcdist, output_calcdist[-1])
    except Exception as e:
        print(e)
        
    # get speed limit area data 
    query = "SELECT * FROM mst_areaspeed_bc"
    df_area = db.get_data(query)
    df_area.columns = ["ID", "GroupArea", "Jalur", "Latitude", "Longitude", "Speed_limit"]
    
    # get location port and mine
    query = "SELECT * FROM mst_portmine_bc"
    df_portmine = db.get_data(query)
    mine_cordinate_1 = df_portmine.iloc[0].to_list()[-2:] # latitude, longitude  
    mine_cordinate_2 = df_portmine.iloc[1].to_list()[-2:] # latitude, longitude  
    port_cordinate = df_portmine.iloc[2].to_list()[-2:] # latitude, longitude
    constrain_distance = 3500 # metres
    
    # get all time orf travel between port and mine
    try:
        df_data = df[df.speed_kmh >= 5]
        output_timetravel = Calc.run_time_travel(df_data, mine_cordinate_1, mine_cordinate_2, port_cordinate, constrain_distance)
        list_time_travel = [tuple(x) for x in output_timetravel[1].values]
        fieldname_timetravel = list(output_timetravel[1].columns.values)
        db.insert_to_db('sdsdata_r_3m_travel_bc', fieldname_timetravel, list_time_travel)
    except Exception as e:
        print(e)
    
    # get the vehicle which break the limit speed of area
    try:
        df_data = df[df.speed_kmh >= df_area.Speed_limit.min()]
        break_speedlimit = Calc.run_speed_area(df, df_area)
        list_break_speedlimit = [tuple(x) for x in break_speedlimit.values]
        fieldname_speedlimit = list(break_speedlimit.columns.values)
        db.insert_to_db('sds_r_3m_speedlimit_bc', fieldname_speedlimit, list_break_speedlimit)
    except Exception as e:
        print(e)
        
    
    print("--- %s minutes ---" % ((time.time() - start_time)/60))
    
    file = open(r"task.txt", "a")
    now = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    file.write(f'{now} - The script ran \n')    
