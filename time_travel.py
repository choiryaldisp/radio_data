# -*- coding: utf-8 -*-
"""
Created on Wed May  4 21:59:22 2022

@author: Lenovo
"""


import pandas as pd

from math import radians, cos, sin, asin, sqrt
import datetime as dt

class TimeTravel:
    def distance(self, lat1, lat2, lon1, lon2):
         
        # The math module contains a function named
        # radians which converts from degrees to radians.
        lon1 = radians(lon1)
        lon2 = radians(lon2)
        lat1 = radians(lat1)
        lat2 = radians(lat2)
          
        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
     
        c = 2 * asin(sqrt(a))
        
        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371
          
        # convert the result from kilometers to meters (result*1000)
        return(c * r)*1000
    
    def mapping_to_dict(self, df_sorted, index, origin, destination, t):
        temp = {
            "callingssi": df_sorted.iloc[index].callingssi,
            "CallingDescr": df_sorted.iloc[index].CallingDescr,
            "origin": origin,
            "destination": destination,
            "departure_time": df_sorted.iloc[index-1].timestamps,
            "arrival_time": df_sorted.iloc[index].timestamps,
            "avg_speed_kmh": (df_sorted.iloc[index].speed_kmh + df_sorted.iloc[index-1].speed_kmh)/2,
            "duration_in_hours": ((df_sorted.iloc[index].timestamps - df_sorted.iloc[index-1].timestamps).total_seconds()/3600)+t,
            "duration_in_minutes": ((df_sorted.iloc[index].timestamps - df_sorted.iloc[index-1].timestamps).total_seconds()/60)+(t*60),
            }
        return temp
    
    def position(self, df_area, df_data):
        dist = 100000
        output = None
        for i in range(len(df_area)):
            temp = self.distance(df_data.latitude, df_area.iloc[i].Latitude, df_data.longitude, df_area.iloc[i].Longitude) 
            if  temp < dist:
                dist = temp
                output = df_area.iloc[i]
                
        return output
    
    def speed_area(self, df_data, df_area):
        df_area_muatan = df_area[df_area.Jalur == 'Muatan']
        df_area_kosongan = df_area[df_area.Jalur == 'Kosongan']
        output = []

        for i in range(len(df_data)):
            if df_data.iloc[i].jalur == 'Muatan':
                output.append(self.position(df_area_muatan, df_data.iloc[i]))
            else:
                output.append(self.position(df_area_kosongan, df_data.iloc[i]))
                
        df_data.reset_index(inplace=True)
        out_df = (pd.concat(output, axis=1)).T
        out_df.rename(columns = {'Latitude':'Latitude_area', 'Longitude':'Longitude_area'}, inplace = True)
        out_df = out_df.reset_index()
        df_join = pd.concat([df_data, out_df], axis=1, join='inner')
        df_join = df_join[['datalength', 'DbId', 'timestamps', 
                           'calledorgdescr', 'callingssi', 'CallingDescr', 
                           'longitude', 'latitude', 'altitude_meter', 
                           'traveldirection', 'positionerror_meter', 
                           'speed_kmh', 'GroupArea', 'Jalur', 
                           'Latitude_area', 'Longitude_area', 'Speed_limit']]
        filter_speed_df = df_join[df_join.speed_kmh > df_join.Speed_limit]
        
        return filter_speed_df
    
    def run(self, df, mine_cordinate_1, mine_cordinate_2, port_cordinate, constrain_distance):
        df_data = df.copy()
        df_data = df_data.astype({'speed_kmh': float})
        (row, col) = df_data.shape
        mine_radius = []
        port_radius = []
        
        df_data['distance_to_mine_1'] = df_data.apply(lambda x: self.distance(x['latitude'], mine_cordinate_1[0], x['longitude'], mine_cordinate_1[1]), axis=1)
        df_data['distance_to_mine_2'] = df_data.apply(lambda x: self.distance(x['latitude'], mine_cordinate_2[0], x['longitude'], mine_cordinate_2[1]), axis=1)
        df_data['distance_to_port'] = df_data.apply(lambda x: self.distance(x['latitude'], port_cordinate[0], x['longitude'], port_cordinate[1]), axis=1)
        
        df_mine = df_data[(df_data.distance_to_mine_1 <= constrain_distance) | (df_data.distance_to_mine_2 <= constrain_distance)]
        df_port = df_data[df_data.distance_to_port <= constrain_distance]
        df_temp = pd.concat([df_mine, df_port]).sort_values(by=['CallingDescr', 'timestamps'], ascending=[True, True], ignore_index=True)
        
        mine_callId = df_mine.CallingDescr.unique().tolist()
        port_callId = df_port.CallingDescr.unique().tolist()

        list_id = []
        for i in mine_callId:
            if i in port_callId:
                list_id.append(i)

        df_listId = df_data[df_data['CallingDescr'].isin(list_id)]
        df_sorted = df_listId.sort_values(by=['CallingDescr', 'timestamps'], ascending=[True, True], ignore_index=True)

        df_to_mine = []
        df_to_port = []

        i = 1
        while i <= len(df_sorted)-1:
            if df_sorted.iloc[i].CallingDescr == df_sorted.iloc[i-1].CallingDescr:
                if df_sorted.iloc[i].distance_to_port <= constrain_distance and (df_sorted.iloc[i-1].distance_to_mine_1 <= constrain_distance or df_sorted.iloc[i-1].distance_to_mine_2 <= constrain_distance):
                    if df_sorted.iloc[i-1].distance_to_mine_1 < df_sorted.iloc[i-1].distance_to_mine_2 :
                        t_port = (df_sorted.iloc[i].distance_to_port/1000)/df_sorted.iloc[i].speed_kmh
                        t_mine1 = (df_sorted.iloc[i].distance_to_mine_1/1000)/df_sorted.iloc[i].speed_kmh
                        temp = self.mapping_to_dict(df_sorted, i, "Mine-1", "Port", t_port+t_mine1)
                        df_to_port.append(temp)
                    else:
                        t_port = (df_sorted.iloc[i].distance_to_port/1000)/df_sorted.iloc[i].speed_kmh
                        t_mine2 = (df_sorted.iloc[i].distance_to_mine_2/1000)/df_sorted.iloc[i].speed_kmh
                        temp = self.mapping_to_dict(df_sorted, i, "Mine-2", "Port", t_port+t_mine2)
                        df_to_port.append(temp)
                    i+=1
                elif (df_sorted.iloc[i].distance_to_mine_1 <= constrain_distance or df_sorted.iloc[i].distance_to_mine_2 <= constrain_distance) and df_sorted.iloc[i-1].distance_to_port <= constrain_distance:
                    if df_sorted.iloc[i].distance_to_mine_1 < df_sorted.iloc[i].distance_to_mine_2:
                        t_port = (df_sorted.iloc[i].distance_to_port/1000)/df_sorted.iloc[i].speed_kmh
                        t_mine1 = (df_sorted.iloc[i].distance_to_mine_1/1000)/df_sorted.iloc[i].speed_kmh
                        temp = self.mapping_to_dict(df_sorted, i,"Port", "Mine-1", t_port+t_mine1)
                        df_to_mine.append(temp)
                    else:
                        t_port = (df_sorted.iloc[i].distance_to_port/1000)/df_sorted.iloc[i].speed_kmh
                        t_mine2 = (df_sorted.iloc[i].distance_to_mine_2/1000)/df_sorted.iloc[i].speed_kmh
                        temp = self.mapping_to_dict(df_sorted, i, "Port", "Mine-2", t_port+t_mine2)
                        df_to_mine.append(temp)
                    i+=1
            i+=1

        df_to_mine = pd.DataFrame(df_to_mine)
        df_to_port = pd.DataFrame(df_to_port)
        output_df = pd.concat([df_to_port, df_to_mine])
        filtered_output_df = output_df[output_df.duration_in_hours <= 3]
        
        df_filtered_dir = []
        for i in range(len(filtered_output_df)):
            df_temp = df_data[
                (df_data.timestamps >= filtered_output_df.iloc[i].departure) & 
                (df_data.timestamps <= filtered_output_df.iloc[i].arrival) & 
                (df_data.callingssi == filtered_output_df.iloc[i].callingssi)]
            if filtered_output_df.iloc[i].Destination == 'Port':
                df_temp['jalur'] = 'Muatan'
            else:
                df_temp['jalur'] = 'Kosongan'
            df_filtered_dir.append(df_temp)

        df_filtered_dir = pd.concat(df_filtered_dir)
        
        return (output_df, filtered_output_df, df_filtered_dir)