# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 13:30:27 2022

@author: Lenovo
"""

import mysql.connector
import pandas as pd
import numpy as np

class DB():
    def __init__(self, host, dbname, user, password):
        self.connection = None
        self.cursor = None
        self.host = host
        self.dbname = dbname
        self.user = user
        self.password = password
    
    def connect_to_db(self):
        try:
            self.connection = mysql.connector.connect(host=self.host,
                                                 database=self.dbname,
                                                 user=self.user,
                                                 password=self.password)
            self.cursor = self.connection.cursor()
        except mysql.connector.Error as e:
            print("Error connect to database", e)
    
    def close_to_db(self):
        if self.connection.is_connected():
            self.connection.close()
            self.cursor.close()
            print("MySQL connection is closed")
            
    def get_data(self, query):
        self.connect_to_db()
        self.cursor.execute(query)
        records = self.cursor.fetchall()
        df_data = pd.DataFrame(records)
        self.close_to_db()
        return df_data
    
    def get_single_data(self, query):
        self.connect_to_db()
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.close_to_db()
        return result[0][0]
    
    def action_row_query(self, query):
        self.connect_to_db()
        self.cursor.execute(query)
        self.connection.commit()
        self.close_to_db()
    
    def insert_to_db(self, tablename, fieldname_list, values_list):
        """
        Parameters
        ----------
        tablename : string
            name of table.
        fieldname_list : list of string
            list all fieldname in table.
        values_list : list of tuple
            list all tuple of value that insert into table.

        Returns
        -------
        None.

        """
        n = len(fieldname_list)
        fieldname_list = ", ".join(fieldname_list)
        string = (', %s'*n)[1:]
        query = "INSERT INTO " + tablename + " (" + fieldname_list + ") VALUES " + " (" + string + ")"
        print(query)
        self.connect_to_db()
        # self.cursor.executemany(query, values_list)
        
        try: 
            # Executing the SQL command
            self.cursor.executemany(query, values_list)
            
            # Commit your changes in the database
            self.connection.commit()
            print('insert data succeed')
        except:
            # Rolling back in case of error
            self.connection.commit()
            print('insert data unsucceed')
        
        self.close_to_db()
        
if __name__ == "__main__":  
    import time
    start_time = time.time()
    
    # df = pd.read_csv("filtered_data - Copy.csv", index_col=0)
    # df.columns = ["datalength", "DbId", "timestamps", "calledorgdescr", "callingssi", "CallingDescr", "longitude", "latitude", "altitude_meter", "traveldirection", "positionerror_meter", "speed_kmh", "remark", "groupdate"]
    # df["speed_kmh"] = pd.to_numeric(df["speed_kmh"], errors='coerce', downcast="float")
    # df = df[(df.datalength != 'NODATA') & (df.speed_kmh >= 10)]
   
    host='34.101.158.193'
    database='tetraflexlogdb'
    user='radioview'
    password='radioview@2022'
    
    db = DB(host, database, user, password)
    
    # get last datetime 
    # query = "SELECT GREATEST(" + \
    #     "(SELECT MAX(timestamps_1) FROM sdsdata_r_3m_distance_bc), " + \
    #     "(SELECT MAX(timestamps_2) FROM sdsdata_r_3m_distance_bc), " + \
    #     "(SELECT MAX(departure_time) FROM sdsdata_r_3m_travel_bc), " + \
    #     "(SELECT MAX(arrival_time) FROM sdsdata_r_3m_travel_bc)" + \
    #     ")"
        
    # last_date = "'" + str(db.get_single_data(query)) + "'"
    
    # df = pd.read_excel("master_data/unitsupport_bc.xlsx")
    # list_data = [tuple(x) for x in df.values]
    # fn = ['nomor_lambung', 'type', 'Dept', 'ISSI', 'Company']
    # db.insert_to_db('mst_unitsupport_bc', fn, list_data)
    query = "SELECT * FROM sdsdata_r_30d_bc WHERE callingssi IN (SELECT ISSI FROM mst_unitsupport_bc) AND datalength <> 'NODATA' AND speed_kmh >= 0 AND timestamps >= '2022-05-31 06:55:12' LIMIT 1000"
    df_data = db.get_data(query)
    df_data.columns = ["datalength", "DbId", "timestamps", "calledorgdescr", "callingssi", "CallingDescr", "longitude", "latitude", "altitude_meter", "traveldirection", "positionerror_meter", "speed_kmh", "remark", "groupdate"]
    df_data.sort_values(by=['timestamps'], inplace=True)
    # df_data.to_csv("filtered_data.csv")
    print("--- %s minutes ---" % ((time.time() - start_time)/60))