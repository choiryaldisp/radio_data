# -*- coding: utf-8 -*-
"""
Created on Wed May 11 20:36:18 2022

@author: Lenovo
"""

import pandas as pd
import numpy as np
from math import radians, cos, sin, asin, sqrt
import datetime as dt
from connect_DB import DB

class CalculateDistance:
    def distance(self, lat1, lat2, lon1, lon2):
         
        # The math module contains a function named
        # radians which converts from degrees to radians.
        lon1 = radians(lon1)
        lon2 = radians(lon2)
        lat1 = radians(lat1)
        lat2 = radians(lat2)
          
        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
     
        c = 2 * asin(sqrt(a))
        
        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371
          
        # calculate the result
        # convert the result from kilometers to meters (result*1000)
        return(c * r)*1000

    def check_same_direction(self, dir1, dir2):
        # south, north, east, west
        temp1 = [0, 0, 0, 0] 
        temp2 = [0, 0, 0, 0]
        
        if 'south' in dir1.lower():
            temp1[0]=1
        elif 'north' in dir1.lower():
            temp1[1]=1
        elif 'east' in dir1.lower():
            temp1[2]=1
        elif 'west' in dir1.lower():
            temp1[3]=1
            
        if 'south' in dir2.lower():
            temp2[0]=1
        elif 'north' in dir2.lower():
            temp2[1]=1
        elif 'east' in dir2.lower():
            temp2[2]=1
        elif 'west' in dir2.lower():
            temp2[3]=1
            
        add = list(np.array(temp1)+np.array(temp2))
        result = False
        for i in add:
            if i > 1:
                result = True
        
        return result
    
    def run(self, df, limit_distance, interval_seconds):
        df_data = df.copy()
        calculate_distance = []
        with_limit_distance = []
        interval_time = dt.timedelta(seconds=interval_seconds) # in seconds
        time_bound = df_data.iloc[0].timestamps
        (row, col) = df_data.shape
        for i in range(row-1):
            if df_data.iloc[i].timestamps > time_bound:
                time_bound += interval_time
            for j in range(i+1, row):
                if df_data.iloc[j].timestamps <= time_bound:
                    if self.check_same_direction(df_data.iloc[i].traveldirection, df_data.iloc[j].traveldirection) and df_data.iloc[i].callingssi != df_data.iloc[j].callingssi:
                        dist = self.distance(df_data.iloc[i].latitude, df_data.iloc[j].latitude, df_data.iloc[i].longitude, df_data.iloc[j].longitude)

                        temp = {}
                        temp['DbId_1'] = df_data.iloc[i].DbId
                        temp['DbId_2'] = df_data.iloc[j].DbId
                        temp['callingssi_1'] = df_data.iloc[i].callingssi
                        temp['callingssi_2'] = df_data.iloc[j].callingssi
                        temp['CallingDescr_1'] = df_data.iloc[i].CallingDescr
                        temp['CallingDescr_2'] = df_data.iloc[j].CallingDescr
                        temp['longitude_1'] = df_data.iloc[i].longitude 
                        temp['latitude_1'] = df_data.iloc[i].latitude
                        temp['longitude_2'] = df_data.iloc[j].longitude 
                        temp['latitude_2'] = df_data.iloc[j].latitude 
                        temp['speed_kmh_1'] = df_data.iloc[i].speed_kmh
                        temp['speed_kmh_2'] = df_data.iloc[j].speed_kmh
                        temp['traveldirection_1'] = df_data.iloc[i].traveldirection
                        temp['traveldirection_2'] = df_data.iloc[j].traveldirection
                        temp['timestamps_1'] = df_data.iloc[i].timestamps
                        temp['timestamps_2'] = df_data.iloc[j].timestamps
                        temp['distance_meter'] = dist
                        calculate_distance.append(temp)
                        if dist < limit_distance:
                            with_limit_distance.append(temp)
                else:
                    break 
                        
        result = pd.DataFrame(calculate_distance)
        under_limit = pd.DataFrame(with_limit_distance)
        df_output = [tuple(x) for x in under_limit.values]
        return result, under_limit, df_output
        
